package 
{
	import adobe.utils.CustomActions;
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.filesystem.File;
	
	/**
	 * ...
	 * @author Christopher Heseltine
	 */
	public class Launcher {
		
		private var _applicationName:String;
		
		public function Launcher() {
			setVersion();
			launchVideoScribe();
		}
		
		public function setVersion():void {
			var appFolder:File = new File(File.desktopDirectory.nativePath + "/VideoScribe");
			var files:Array = appFolder.getDirectoryListing();
			var appFileNames:Array = new Array();
			var versions:Array = new Array();
			
			for each (var file:File in files) {
				if (isApplication(file)) {
					appFileNames.push(file.name);
				}
			}
			
			for each (var fileName:String in appFileNames) {
				var startIndex:int = fileName.indexOf("_") + 1;
				var endIndex:int = fileName.lastIndexOf(".");
				var version:String = fileName.substring(startIndex, endIndex);
				
				versions.push(version);
			}
			
			versions.sort(Array.DESCENDING);
			
			_applicationName = "VideoScribe_" + versions[0] + ".exe";
		}
		
		private function isApplication(file:File):Boolean {
			return file.name.indexOf("VideoScribe") >= 0 && (file.extension == "exe" || file.extension == "dmg");
		}
		
		private function launchVideoScribe():void {
			if (!NativeProcess.isSupported)
				throw new Error("Native process is not supported, can't launch VideoScribe");
			
			if (_applicationName == null)
				throw new Error("Application name is not set");
			
			var app:File = File.desktopDirectory.resolvePath("VideoScribe/" + _applicationName);
			
			if (!app.exists)
				throw new Error("VideoScribe application could not be found");
			
			var startupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			var nativeProcess:NativeProcess = new NativeProcess();
			startupInfo.executable = app;
			
			nativeProcess.start(startupInfo);
		}
	}
}