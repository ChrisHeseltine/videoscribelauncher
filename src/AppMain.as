package 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Christopher Heseltine
	 */
	public class AppMain extends MovieClip{
		
		public function AppMain() {
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var launcher:Launcher = new Launcher();
			stage.nativeWindow.close();
		}
	}
}